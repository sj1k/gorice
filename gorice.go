package gorice

import (
	"os"
	"path/filepath"
)

const (
	RiceExt  = ".rice"
	RiceMode = 0664
)

type SymPath string

func (s SymPath) String() string {
	sym, err := filepath.EvalSymlinks(string(s))
	if err != nil {
		return string(s)
	}
	return sym
}

// Rice is a struct that contains 2 os.File pointers,
// It reads from one and writes to the other. Crazy right?
type Rice struct {
	Template *os.File // The template file to read from.
	File     *os.File // The original file to write to.
}

// NewRice attempts to open path for writing, and path + RiceExt for reading.
// In other words this opens 2 files, one with a (by default .rice) extension on the same path.
func NewRice(path string) (Rice, error) {
	var mode os.FileMode
	mode = RiceMode
	sym := SymPath(path).String()
	stat, err := os.Stat(sym)
	if err == nil {
		os.Remove(sym)
		mode = stat.Mode()
	}
	file, err := os.OpenFile(sym, os.O_RDWR|os.O_CREATE, mode)
	if err != nil {
		return Rice{}, err
	}
	template, err := os.OpenFile(SymPath(path+RiceExt).String(), os.O_RDWR|os.O_CREATE, mode)
	return Rice{
		Template: template,
		File:     file,
	}, err
}

// Read reads from the underlying Template, usually a .rice file in the same directory.
func (r Rice) Read(p []byte) (int, error) {
	return r.Template.Read(p)
}

// Close closes both of the underlying files.
func (r Rice) Close() error {
	err := r.Template.Close()
	if err != nil {
		return err
	}
	return r.File.Close()
}

// Write writes to the underlying File, the original file.
func (r Rice) Write(p []byte) (int, error) {
	return r.File.Write(p)
}
