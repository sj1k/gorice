# Example

This readme is actually generated by the `README.md.rice` file / the data files in this directory.

Run this command from the parent directory

```
grice -w -d examples/README.md
```


Then modify some values in the .rice file or some of the data files!,
the README.md will update after each time you save.


## Output

| Key   | Value |
|-------|-------|
| background | #FFFFFF
| items | [one two three]
| thing | value
