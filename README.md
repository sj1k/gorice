# gorice

A basic dot file templating system. I leverage go's templating engine for functions such as "include" for loading arbitrary variable files.

Having an include system lets me track requirements and reload only the parts that change. This means if you change a variable file that is used in 2 files, it will change those 2 files.


# Installing

```
go install gitlab.com/sj1k/gorice/cmd/grice
```


# Running

`grice -d ~/.path/to/dotfile -d ...`


This loads `~/.path/to/dotfile.rice` as a template and fills the values into the original dotfile.


It appends .rice to the path **before** evaluating any symlinks, so `dotfile` is a symlink but `dotfile.rice` is not.
This is useful for making a self contained dots folder with symlinks linking out to the original dotfile locations.


# Include

Ill cover the include function here,

Basically its a cached handler for loading arbitrary vars. (See support section below)

It will also keep track of what files include what vars, letting gorice reload only what you've changed (per-file not per-variable).
 
Ill cover 2 methods, if you're familiar with go's templates this will look familiar.


## Method 1 - With

This was not intentional but it works really well with my system.

```
{{ with include "path/to/vars.yaml" }}
   {{ .variable }}
{{ end }}
```

## Method 2 - $variables

This is less awesome, but way more useful.

```
{{ $vars := include "path/to/vars.yaml" }}
{{ $vars.variable }}
```

## Include support

Here is a complete list of the file types include can load.
Feel free to request more or help me implement more!

- [x] .json
- [x] .yaml
