package watcher

import (
	"os"
	"time"
)

type FileWatcher struct {
	updates chan Status
	addChan chan WatchInfo
	remChan chan string
}

type WatchInfo struct {
	Origin string
	Path   string
}

func NewFileWatcher() (FileWatcher, chan Status) {
	updates := make(chan Status)
	adding := make(chan WatchInfo)
	removing := make(chan string)
	return FileWatcher{
		updates: updates,
		addChan: adding,
		remChan: removing,
	}, updates
}

func (w FileWatcher) Add(path string, origin string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}
	_, err = os.Stat(origin)
	if err != nil {
		return err
	}
	w.addChan <- WatchInfo{
		Path:   path,
		Origin: origin,
	}
	return nil
}

func (w FileWatcher) Watch(rate time.Duration) {
	times := make(map[string]time.Time)
	files := make(map[string][]string)

	ticker := time.NewTicker(rate)
	for {
		select {
		case info := <-w.addChan:
			ostat, _ := os.Stat(info.Origin)
			pstat, _ := os.Stat(info.Path)
			files[info.Origin] = append(files[info.Origin], info.Path)
			times[info.Origin] = ostat.ModTime()
			times[info.Path] = pstat.ModTime()

		case path := <-w.remChan:
			files = removePath(files, path)

		case <-ticker.C:

			for origin, paths := range files {
				for _, path := range paths {
					w.checkFile(times, path, origin)
				}
			}
		}
	}
}

func removePath(files map[string][]string, path string) map[string][]string {
	output := make(map[string][]string)
	for origin, paths := range files {
		for _, p := range paths {
			if p != path {
				output[origin] = append(output[origin], p)
			}
		}
	}
	return output
}

func (w FileWatcher) checkFile(times map[string]time.Time, path string, origin string) {
	stat, err := os.Stat(path)
	if err != nil {
		w.updates <- Status{
			Path:   path,
			Origin: origin,
			Code:   StatusRemoved,
		}
		return
	}

	if time, ok := times[path]; ok && stat.ModTime() != time {
		w.updates <- Status{
			Path:   path,
			Origin: origin,
			Code:   StatusModified,
		}
		times[path] = stat.ModTime()
	}
}

func (w FileWatcher) Remove(paths ...string) {
	for _, path := range paths {
		w.remChan <- path
	}
}

func (w FileWatcher) Updates() chan Status {
	return w.updates
}
