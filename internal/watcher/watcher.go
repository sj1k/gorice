package watcher

import (
	"time"
)

const (
	StatusModified int = iota
	StatusRemoved
	StatusNotFound
)

type Status struct {
	Code   int
	Path   string
	Origin string
}

type Adder interface {
	Add(origin string, path string) error
}

type Remover interface {
	Remove(paths ...string)
}

type Watcher interface {
	Watch(rate time.Duration)
}

type Updater interface {
	Updates() chan Status
}

type WatchManager interface {
	Adder
	Remover
	Watcher
	Updater
}
