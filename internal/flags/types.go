package flags

import (
	"fmt"
	"path/filepath"
)

// FilePath is a type for being used in flag parsing.
type FilePath string

func NewFilePath(path string) FilePath {
	var f FilePath
	f.Set(path)
	return f
}

// Set attempts to set the abs path, falling back on input.
func (f *FilePath) Set(v string) error {
	abs, err := filepath.Abs(v)
	if err == nil {
		v = abs
	}
	*f = FilePath(v)
	return nil
}

func (f FilePath) String() string {
	return string(f)
}

// FileList is meant to be used with flag parsing, however it appends
// each value so you can use the flag multiple times for many files.
// This type supports globs for multiple files at once.
// TODO: Add glob support.
type FileList []FilePath

func (f *FileList) Set(v string) error {
	var file FilePath
	err := file.Set(v)
	if err != nil {
		return err
	}
	*f = append(*f, file)
	return nil
}

func (f FileList) String() string {
	var output string
	for _, value := range f {
		output += fmt.Sprintf("%s ", value)
	}
	// TODO: May need some rtrim here to remove a trailing space.
	// But not too worried.
	return output
}
