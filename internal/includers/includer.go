package includers

// Includer is used to dynamically load arbitrary data from inside templates.
// This takes 2 strings, one is 'origin' this is supposed to be the file you import from.
// This function is not directly exposed to the templates as "include",
// The templates "include" is a small wrapper that appends 'origin' automagically so that is not required in the templates.
type Includer interface {
	Include(path string, origin string) interface{}
}

// Cache should return the complete cached contents for all of the variables.
// .Cache() is used by the built in "include" function for the templates.
type Cacher interface {
	Cache() map[string]interface{}
}

// Included should return a map of name / path keys with a slice of paths for the values.
// This is supposed to be a map of dot files, containing a slice of each variable file it requires.
type Included interface {
	Included() map[string][]string
}

type Clearer interface {
	Clear() bool
}

// IncludeManager handles including and managing files.
// The idea is to be able to load your variables for dotfiles from within the template.
// This gives a few advantages over passing them into the program as flags.
// Such as; The ability to track requirements, as in, I know what dot files require which variables.
// This lets you reload specific dot files and systems when only those variables have changed.
type IncludeManager interface {
	Includer
	Included
	Cacher
	Clearer
}
