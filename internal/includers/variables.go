package includers

import (
	"fmt"
	"io/ioutil"
	"path"
)

type Parser func([]byte) interface{}

type VarIncluder struct {
	Paths   map[string][]string
	Parsers map[string]Parser
	Cached  map[string]interface{}

	origin string
}

func NewVarIncluder(parsers map[string]Parser) *VarIncluder {
	paths := make(map[string][]string)
	cached := make(map[string]interface{})
	return &VarIncluder{
		Paths:   paths,
		Parsers: parsers,
		Cached:  cached,
	}
}

func (v VarIncluder) Include(fpath string, origin string) interface{} {
	var output interface{}
	if value, ok := v.Cached[fpath]; ok {
		v.SavePath(fpath, origin)
		return value
	}
	data, err := ioutil.ReadFile(fpath)
	if err != nil {
		return err.Error()
	}
	ext := path.Ext(fpath)
	parser, ok := v.Parsers[ext]
	if !ok {
		return fmt.Sprintf("Could not parse '%s'", ext)
	}
	output = parser([]byte(data))
	v.Cached[fpath] = output
	v.SavePath(fpath, origin)
	return output
}

func (v VarIncluder) SavePath(fpath string, origin string) {
	if _, ok := v.Paths[origin]; !ok {
		v.Paths[origin] = make([]string, 0)
	}
	v.Paths[origin] = append(v.Paths[origin], fpath)
}

func (v VarIncluder) Included() map[string][]string {
	return v.Paths
}

func (v VarIncluder) Cache() map[string]interface{} {
	return v.Cached
}

func (v *VarIncluder) Clear() bool {
	v.Cached = make(map[string]interface{})
	v.Paths = make(map[string][]string)
	return true
}
