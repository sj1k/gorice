package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"time"

	"gitlab.com/sj1k/gorice/internal/flags"
	"gitlab.com/sj1k/gorice/internal/includers"
	"gitlab.com/sj1k/gorice/internal/watcher"

	yaml "gopkg.in/yaml.v2"
)

// Flags stores all the flags passed to this program.
// I also store stdin here because why not?
type Flags struct {
	Dots  flags.FileList
	Rate  time.Duration
	Watch bool
	Stdin []byte
}

func ParseFlags() *Flags {
	var dots flags.FileList
	var rate int
	var watch bool

	flag.Var(&dots, "d", "Your dot files, a path to any file or symlink.")
	flag.IntVar(&rate, "r", 1, "The frequency of file update checks, in seconds.")
	flag.BoolVar(&watch, "w", false, "Activates the watch loop, this re-templates after any file changes.")
	flag.Parse()

	return &Flags{
		Dots:  dots,
		Rate:  time.Duration(rate) * time.Second,
		Watch: watch,
	}
}

func main() {
	var err error

	parsers := map[string]includers.Parser{
		".yaml": loadYAML,
		".json": loadJSON,
	}

	includer := includers.NewVarIncluder(parsers)
	watcher, _ := watcher.NewFileWatcher()

	flags := ParseFlags()

	if flags.Watch {
		err = Watch(flags, includer, watcher)
	} else {
		err = Load(flags, includer)
	}

	if err != nil {
		fmt.Println(err.Error())
	}
}

func loadYAML(data []byte) interface{} {
	var output interface{}
	yaml.Unmarshal(data, &output)
	return output
}

func loadJSON(data []byte) interface{} {
	var output interface{}
	json.Unmarshal(data, &output)
	return output
}
