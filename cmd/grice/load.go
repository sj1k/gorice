package main

import (
	"gitlab.com/sj1k/gorice/internal/includers"
)

func Load(f *Flags, vars includers.IncludeManager) error {
	_, err := TemplateDots(f.Dots, vars)
	return err
}
