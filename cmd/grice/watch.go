package main

import (
	"fmt"
	"log"
	"path/filepath"

	"gitlab.com/sj1k/gorice"
	"gitlab.com/sj1k/gorice/internal/flags"
	"gitlab.com/sj1k/gorice/internal/includers"
	"gitlab.com/sj1k/gorice/internal/watcher"
)

type Includes map[string][]string

func Watch(f *Flags, vars includers.IncludeManager, manager watcher.WatchManager) error {
	included, err := TemplateDots(f.Dots, vars)
	if err != nil {
		return err
	}

	go manager.Watch(f.Rate)

	for _, file := range f.Dots {
		template := fmt.Sprintf("%s%s", file, gorice.RiceExt)
		manager.Add(template, string(file))
	}

	for origin, files := range included {
		for _, file := range files {
			manager.Add(file, origin)
		}
	}

	updates := manager.Updates()

	for {
		update := <-updates

		rel, err := filepath.Rel(update.Path, update.Origin)
		if err == nil {
			log.Printf("? %s > %s", update.Path, rel)
		} else {
			log.Println("?", update.Path)
		}

		vars.Clear()

		incld, err := TemplateDots(flags.FileList{flags.NewFilePath(update.Origin)}, vars)

		if err != nil {
			log.Println("!", err.Error())
		}

		added, removed := includedDifference(incld[update.Origin], included[update.Origin])

		for _, file := range added {
			log.Println("\t+", file)
			manager.Add(file, update.Origin)
		}

		for _, file := range removed {
			log.Println("\t-", file)
			manager.Remove(file)
		}

		included[update.Origin] = incld[update.Origin]

	}
}

func includedDifference(current []string, previous []string) ([]string, []string) {
	var added []string
	var removed []string
	for _, file := range current {
		if !isIncluded(previous, file) {
			added = append(added, file)
		}
	}
	for _, file := range previous {
		if !isIncluded(current, file) {
			removed = append(removed, file)
		}
	}
	return added, removed
}

func flattenInclude(included Includes) []string {
	var flat []string
	for _, files := range included {
		flat = append(flat, files...)
	}
	return flat
}

func isIncluded(included []string, path string) bool {
	for _, item := range included {
		if item == path {
			return true
		}
	}
	return false
}
