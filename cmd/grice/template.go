package main

import (
	"html/template"
	"io/ioutil"
	"os/exec"
	"path/filepath"

	"gitlab.com/sj1k/gorice"
	"gitlab.com/sj1k/gorice/internal/flags"
	"gitlab.com/sj1k/gorice/internal/includers"
)

func TemplateDots(dots flags.FileList, vars includers.IncludeManager) (Includes, error) {
	var included Includes
	var err error

	defered := make([]*exec.Cmd, 0)

	defr := bashDefer(&defered)

	for _, dot := range dots {
		file := string(dot)
		include := originIncluder(vars, file)
		funcs := template.FuncMap{
			"bash":    bash,
			"defer":   defr,
			"include": include,
		}

		rice, err := gorice.NewRice(file)
		if err != nil {
			return included, err
		}
		defer rice.Close()

		tmpl, err := prepareTemplates(rice, funcs)
		if err != nil {
			return included, err
		}

		err = tmpl.Execute(rice, vars)
		if err != nil {
			return included, err
		}
	}
	for _, def := range defered {
		go def.Run()
	}
	return vars.Included(), err
}

func bash(cmd string, arg ...string) string {
	command := exec.Command(cmd, arg...)
	output, err := command.Output()
	if err != nil {
		return err.Error()
	}
	return string(output)
}

func prepareTemplates(rice gorice.Rice, funcs template.FuncMap) (*template.Template, error) {
	tmp := template.New(rice.Template.Name()).Funcs(funcs)
	data, err := ioutil.ReadAll(rice)
	if err != nil {
		return nil, err
	}
	tmp, err = tmp.Parse(string(data))
	return tmp, err
}

func originIncluder(vars includers.IncludeManager, origin string) func(path string) interface{} {
	return func(path string) interface{} {
		abs, err := filepath.Abs(path)
		if err == nil {
			path = abs
		}
		return vars.Include(path, origin)
	}
}

func bashDefer(defered *[]*exec.Cmd) func(cmd string, arg ...string) string {
	return func(cmd string, arg ...string) string {
		command := exec.Command(cmd, arg...)
		*defered = append(*defered, command)
		return ""
	}
}
